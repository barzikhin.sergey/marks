# Marks
### Contents

* [Getting started](#getting-started)
* [Available modes](#available-modes)

#### Getting started

Marks allows you to analyze data files with the csv extension, which are compiled strictly according to the following format:
```
{date};{student's last name};{subject};{teacher's last name};{grade}
```
You can run the program via console mode by passing the name of yourfile as an additional startup parameter, for example "python main,py marks.csv" or simply run the program using the command "python main,py". Next, you need to enter the analysis mode and parameters, if necessary. For example, you can use such a sequence of programs from the moment the application starts running:
```
python main,py marks.csv
student Ivanin
```

#### Available modes

Marks allows you to use different modes for data analysis, here is a complete list of them with the necessary parameters. Where an additional parameter is not specified, it is not required.

* student {surname}
  * allows you to get average student grades for each of the subjects;

* teacher {surname}
  * allows you to get the average grades that the teacher gives for each of the subjects;

* worst_teacher
  * allows you to get the last name of the teacher and the subject in which the average score of all students is the lowest;

* best_student
  * allows you to get the last name of the student who has the best average score in all subjects;

* easiest_course
  * allows you to get the name of the course for which the average score of all students is the highest;

* best_students_on_courses
  * allows you to get the name of the best student in each course (by average score);