from os import path
from exceptions import *
from school import *


def is_file_csv_exists(filename: str = "") -> bool:
    if filename.endswith(".csv") and path.exists(filename):
        return True
    else:
        return False


def get_avg_mark(marks_list: list[Mark]) -> float:
    return round(sum([mark.value for mark in marks_list]) / len(marks_list), 2)


class ReadFile:
    def __init__(self, filename: str = ""):
        self.filename = filename

    def read_csv(self) -> list:
        validated_data = []
        with open(self.filename, "r", encoding="utf-8") as csv_file:
            for row in csv_file:
                if ReadFile.is_valid_data(row):
                    validated_data.append(row)
        return validated_data

    @staticmethod
    def is_valid_data(data_line: str) -> bool:
        data_list = data_line.split(";")
        if len(data_list) != 5:
            return False
        for data_cell in data_list:
            if not data_cell:
                return False
        if 5 >= int(data_list[4]) >= 2:
            return True
        else:
            return False


class SchoolData:
    def __init__(self, data):
        self.data = data
        self.student_list = []
        self.teacher_list = []
        self.subject_list = []

    def sort_data(self):
        for data_line in self.data:
            data_cell = data_line.split(";")
            mark = Mark(value=int(data_cell[4]))
            self.student_list_append(
                surname=data_cell[1],
                subject_name=data_cell[2],
                mark=mark)
            self.teacher_list_append(
                surname=data_cell[3],
                subject_name=data_cell[2],
                mark=mark)
            self.subject_list_append(
                subject_name=data_cell[2],
                mark=mark)

    def student_list_append(self, surname: str, subject_name: str, mark: Mark):
        is_student_exist = False
        for student in self.student_list:
            if surname in student.surname:
                is_student_exist = True
                student.append_mark_to_subject(subject_name, mark)
        if not is_student_exist:
            new_student = Student(surname=surname,
                                  subject_name_to_marks={subject_name: [mark]})
            self.student_list.append(new_student)

    def teacher_list_append(self, surname: str, subject_name: str, mark: Mark):
        is_teacher_exist = False
        for teacher in self.teacher_list:
            if surname in teacher.surname:
                is_teacher_exist = True
                teacher.append_mark_to_subject(subject=subject_name, mark=mark)
        if not is_teacher_exist:
            new_teacher = Teacher(surname=surname,
                                  subject_name_to_marks={subject_name: [mark]})
            self.teacher_list.append(new_teacher)

    def subject_list_append(self, subject_name: str, mark: Mark):
        is_subject_exist = False
        for subject in self.subject_list:
            if subject_name in subject.subject_name:
                is_subject_exist = True
                subject.append_mark_to_subject(mark=mark)
        if not is_subject_exist:
            new_subject = Subject(subject_name=subject_name,
                                  subject_name_to_marks={subject_name: [mark]})
            self.subject_list.append(new_subject)


class ModeHandler:
    available_commands = {
        "student": "Format: 'student [student name]'. The average grade in "
                   "each subject",
        "teacher": "Format: 'student [student name]'. The "
                   "average grade given by the teacher",
        "worst_teacher": "the name of the teacher and the subject in which "
                         "the average score of all students is the lowest",
        "best_student": "the last name of the student who has "
                        "the highest average scores in all "
                        "subjects is displayed",
        "easiest_course": "the name of the course in which all students have "
                          "the highest scores",
        "best_students_on_course": "displays the best student in each course",
    }

    def __init__(self, data: SchoolData):
        self.data = data

    def infinite_pooling(self):
        while True:
            try:
                user_command = self.get_command().split()
                if len(user_command) == 1:
                    mode = user_command[0]
                    if mode == "worst_teacher":
                        self.print_result(self.worst_teacher())
                    elif mode == "best_student":
                        self.print_result(self.best_student())
                    elif mode == "easiest_course":
                        self.print_result(self.easiest_course())
                    elif mode == "best_students_on_courses":
                        self.print_result(self.best_students_on_courses())
                elif len(user_command) == 2:
                    mode, name = user_command
                    if mode == "student":
                        self.print_result(self.student(name))
                    elif mode == "teacher":
                        self.print_result(self.teacher(name))
                else:
                    raise CommandDoesNotExist
            except CommandDoesNotExist:
                print("There is no such command")

    def get_command(self) -> str:
        user_command = input("Enter mode and parameter "
                             "(if requires): ")
        if user_command.split()[0] in self.available_commands.keys():
            return user_command
        else:
            raise CommandDoesNotExist

    def print_result(self, data: dict, amount_value_tabulation=0):
        for key, value in data.items():
            print("{tab}{key}:".format(
                tab="\t" * amount_value_tabulation,
                key=key,
            ))
            if isinstance(value, (dict, list)):
                self.print_result(value, amount_value_tabulation + 1)
            else:
                print("{tab}{value}".format(
                    tab="\t" * (amount_value_tabulation + 1),
                    value=value,
                ))

    def help(self):
        for command, description in self.available_commands.items():
            print(f"\t{command} - {description}", end="\n")

    def student(self, student_surname: str) -> dict[str: int]:
        result = {"name": student_surname}
        for student in self.data.student_list:
            if student_surname == student.surname:
                for subject_name, mark_list in\
                        student.subject_name_to_marks.items():
                    result[subject_name] = get_avg_mark(mark_list)
        return result

    def teacher(self, teacher_surname: str) -> dict[str: int]:
        result = {"name": teacher_surname}
        for teacher in self.data.teacher_list:
            if teacher_surname == teacher.surname:
                for subject_name, mark_list in\
                        teacher.subject_name_to_marks.items():
                    result[subject_name] = get_avg_mark(mark_list)
        return result

    def worst_teacher(self) -> dict[str: int]:
        worst_subject_name_to_avg_mark = ModeHandler.worst_subject(
            self.data.teacher_list[0].subject_name_to_marks
        )
        worst_teacher_surname = self.data.teacher_list[0].surname

        for teacher in self.data.teacher_list:
            subject_name_to_avg_mark = ModeHandler.worst_subject(
                teacher.subject_name_to_marks
            )
            teacher_surname = teacher.surname
            if subject_name_to_avg_mark[1] < worst_subject_name_to_avg_mark[1]:
                worst_subject_name_to_avg_mark = subject_name_to_avg_mark
                worst_teacher_surname = teacher_surname

        return {worst_teacher_surname: worst_subject_name_to_avg_mark[0]}

    def best_student(self) -> dict[str: int]:
        best_subject_name_to_avg_mark = ModeHandler.best_subject(
            self.data.student_list[0].subject_name_to_marks
        )
        best_student_surname = self.data.student_list[0].surname

        for student in self.data.student_list:
            subject_name_to_avg_mark = ModeHandler.best_subject(
                student.subject_name_to_marks)
            student_surname = student.surname
            if subject_name_to_avg_mark[1] > best_subject_name_to_avg_mark[1]:
                best_subject_name_to_avg_mark = subject_name_to_avg_mark
                best_student_surname = student_surname

        return {best_student_surname: best_subject_name_to_avg_mark[0]}

    def easiest_course(self) -> dict[str: int]:
        best_subject_name_to_avg_mark = ModeHandler.best_subject(
            self.data.subject_list[0].subject_name_to_marks
        )
        best_subject_name = self.data.subject_list[0].subject_name

        for subject in self.data.subject_list:
            subject_name_to_avg_mark = ModeHandler.best_subject(
                subject.subject_name_to_marks)
            subject_name = subject.subject_name
            if subject_name_to_avg_mark[1] > best_subject_name_to_avg_mark[1]:
                best_subject_name_to_avg_mark = subject_name_to_avg_mark
                best_subject_name = subject_name

        return {best_subject_name: best_subject_name_to_avg_mark[0]}

    def best_students_on_courses(self) -> dict[str: str]:
        subject_name_list = []
        for subject in self.data.subject_list:
            subject_name_list.append(subject.subject_name)

        result = {}
        for subject_name in subject_name_list:
            best_avg_mark_on_course = 0
            for student in self.data.student_list:
                if subject_name in student.subject_name_to_marks.keys():
                    student_avg_mark_on_course = get_avg_mark(
                        student.subject_name_to_marks[subject_name]
                    )
                    if student_avg_mark_on_course > best_avg_mark_on_course:
                        best_avg_mark_on_course = student_avg_mark_on_course
                        result[subject_name] = student.surname
        return result

    @staticmethod
    def best_subject(
            subject_name_to_marks: dict[str, list[Mark]]) -> tuple[str, float]:
        subject_name_to_marks_list = []
        for subject_name, mark_list in subject_name_to_marks.items():
            subject_name_to_marks_list.append(
                (subject_name, get_avg_mark(mark_list))
            )

        best_subject = subject_name_to_marks_list[0]
        for subject_name, avg_mark in subject_name_to_marks_list:
            if best_subject[1] < avg_mark:
                best_subject = (subject_name, avg_mark)

        return best_subject

    @staticmethod
    def worst_subject(
            subject_name_to_marks: dict[str, list[Mark]]) -> tuple[str, float]:
        subject_name_to_marks_list = []
        for subject_name, mark_list in subject_name_to_marks.items():
            subject_name_to_marks_list.append(
                (subject_name, get_avg_mark(mark_list))
            )

        worst_subject = subject_name_to_marks_list[0]
        for subject_name, avg_mark in subject_name_to_marks_list:
            if worst_subject[1] < avg_mark:
                worst_subject = (subject_name, avg_mark)

        return worst_subject
