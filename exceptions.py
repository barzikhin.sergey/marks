class CommandDoesNotExist(Exception):
    pass


class NameDoesNotExist(Exception):
    pass
