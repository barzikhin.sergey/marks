from core import *
from sys import argv


def run_app(filename=""):
    while True:
        if is_file_csv_exists(filename=filename):
            rf = ReadFile(filename=filename)
            break
        else:
            filename = input("Enter file name: ")
    validated_data = rf.read_csv()
    school_data = SchoolData(data=validated_data)
    school_data.sort_data()
    mode = ModeHandler(school_data)
    mode.infinite_pooling()


if __name__ == "__main__":
    if len(argv) == 2:
        run_app(argv[1])
    else:
        run_app()
