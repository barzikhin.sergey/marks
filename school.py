from typing import Optional, Union


class Mark:
    def __init__(self, value: Optional[int] = None):
        self.__value = value

    @property
    def value(self) -> int:
        return self.__value

    @value.setter
    def value(self, value: Union[str, int]):
        self.__value = value


class Student:
    def __init__(self, surname: str, subject_name_to_marks:
                 Optional[dict[str, list[Mark]]] = None):
        self.__surname = surname
        self.__subject_name_to_marks = subject_name_to_marks \
            if subject_name_to_marks else {}

    @property
    def surname(self) -> str:
        return self.__surname

    @surname.setter
    def surname(self, new_surname: str):
        self.__surname = new_surname

    @property
    def subject_name_to_marks(self) -> dict:
        return self.__subject_name_to_marks

    @subject_name_to_marks.setter
    def subject_name_to_marks(self, new_subject_name_to_marks: dict):
        self.__subject_name_to_marks = new_subject_name_to_marks

    def append_mark_to_subject(self, subject: str, mark: Mark):
        if not self.subject_name_to_marks.get(subject):
            self.subject_name_to_marks[subject] = [mark]
        else:
            self.subject_name_to_marks[subject].append(mark)


class Teacher:
    def __init__(self, surname: str, subject_name_to_marks:
                 Optional[dict[str, list[Mark]]] = None):
        self.__surname = surname
        self.__subject_name_to_marks = subject_name_to_marks \
            if subject_name_to_marks else {}

    @property
    def surname(self) -> str:
        return self.__surname

    @surname.setter
    def surname(self, new_surname: str):
        self.__surname = new_surname

    @property
    def subject_name_to_marks(self) -> dict:
        return self.__subject_name_to_marks

    @subject_name_to_marks.setter
    def subject_name_to_marks(self, new_subject_name_to_marks: dict):
        self.__subject_name_to_marks = new_subject_name_to_marks

    def append_mark_to_subject(self, subject: str, mark: Mark):
        if not self.subject_name_to_marks.get(subject):
            self.subject_name_to_marks[subject] = [mark]
        else:
            self.subject_name_to_marks[subject].append(mark)


class Subject:
    def __init__(self, subject_name: str, subject_name_to_marks:
                 Optional[dict[str, list[Mark]]] = None):
        self.__subject_name = subject_name
        self.__subject_name_to_marks = subject_name_to_marks \
            if subject_name_to_marks else {}

    @property
    def subject_name(self) -> str:
        return self.__subject_name

    @subject_name.setter
    def subject_name(self, new_subject_name: str):
        self.__subject_name = new_subject_name

    @property
    def subject_name_to_marks(self) -> dict:
        return self.__subject_name_to_marks

    @subject_name_to_marks.setter
    def subject_name_to_marks(self, new_subject_name_to_marks: dict):
        self.__subject_name_to_marks = new_subject_name_to_marks

    def append_mark_to_subject(self, mark: Mark):
        if not self.subject_name_to_marks.get(self.subject_name):
            self.subject_name_to_marks[self.subject_name] = [mark]
        else:
            self.subject_name_to_marks[self.subject_name].append(mark)
